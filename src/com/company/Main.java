package com.company;

public class Main {

    public static void main(String[] args){
        Hewan kucing = new Kucing(4, "paru-paru");
        Hewan ayam = new Ayam(2, "paru-paru");
        Hewan ikanLele = new Ikan("Lele",0, "insang", "air tawar");
        Hewan ikanPaus = new Ikan("Paus",0, "paru-paru", "air laut");

        kucing.tampilkan();
        ayam.tampilkan();
        ikanLele.tampilkan();
        ikanPaus.tampilkan();
    }


}





