package com.company;

public class Ikan extends Hewan {

    private String habitat;
    private String nama;

    public Ikan(String nama, int kaki, String alatNafas, String habitat){
        super(kaki,alatNafas);
        this.nama=nama;
        this.habitat=habitat;
    }

    public void setHabitat(String habitat){
        habitat=habitat;
    }

    public String getHabitat(){
        return habitat;
    }

    public void setNama(String nama){
        nama=nama;
    }

    public String getNama(){
        return nama;
    }

    @Override
    public void tampilkan(){
        System.out.println("Ikan "+getNama()+" memiliki "+getKaki()+" kaki dan bernafas dengan "+getAlatNafas()+" dan hidup di "+getHabitat());
    }
}