package com.company;

public class Ayam extends Hewan {

    public Ayam(int kaki, String alatNafas){
        super(kaki,alatNafas);
    }

    @Override
    public void tampilkan(){
        System.out.println("Ayam memiliki "+getKaki()+" kaki dan bernafas dengan "+getAlatNafas());
    }
}