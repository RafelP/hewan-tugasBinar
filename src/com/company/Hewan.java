package com.company;

public abstract class Hewan {

    private int kaki;
    private String alatNafas;

    public Hewan(int kaki, String alatNafas){
        this.kaki=kaki;
        this.alatNafas=alatNafas;
    }

    public void setKaki(int kaki){
        kaki=kaki;
    }

    public int getKaki(){
        return kaki;
    }

    public void setAlatNafas(String alatNafas){
        alatNafas=alatNafas;
    }

    public String getAlatNafas(){
        return alatNafas;
    }

    public abstract void tampilkan();
}