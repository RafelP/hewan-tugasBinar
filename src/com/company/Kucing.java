package com.company;


public class Kucing extends Hewan {

    public Kucing(int kaki, String alatNafas){
        super(kaki,alatNafas);
    }

    @Override
    public void tampilkan(){
        System.out.println("Kucing memiliki "+getKaki()+" kaki dan bernafas dengan "+getAlatNafas());
    }
}
