package com.example.rafika.project;


public class Main {

    public static void main(String[] args) {
        Hewan kucing = new Kucing(4,"paru-paru");
        Hewan ayam = new Ayam(2,"paru-paru");
        Hewan ikanTuna = new Ikan("Tuna",0,"insang","Air tawar");
        Hewan ikanMujair = new Ikan("Mujair",0,"insang", "Air laut");

        kucing.print();
        ayam.print();
        ikanTuna.print();
        ikanMujair.print();
    }
}
