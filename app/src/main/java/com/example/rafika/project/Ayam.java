package com.example.rafika.project;

public class Ayam extends Hewan {
    public Ayam(int kaki, String alatNafas) {
        super(kaki, alatNafas);
    }
    public void print(){
        System.out.println("Ayam memiliki : "+super.getKaki()+", Bernafas dengan : "+super.getAlatNafas());
    }

}
