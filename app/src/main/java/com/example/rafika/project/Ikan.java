package com.example.rafika.project;

public class Ikan extends Hewan {
    private String habitat;
    private  String nama;

    public Ikan(String nama, int kaki, String alatNafas, String habitat) {
        super(kaki, alatNafas);
        this.habitat=habitat;
    }

    public void setHabitat(String habitat){
        this.habitat=habitat;
    }

    public String getHabitat() {
        return habitat;
    }

    public void print(){
        System.out.println("Ikan Tuna memiliki : "+super.getKaki()+", Bernafas dengan : "+super.getAlatNafas()+", Habitat di : "+getHabitat());
    }
}
